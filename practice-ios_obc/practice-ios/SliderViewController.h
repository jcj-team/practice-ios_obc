//
//  SliderViewController.h
//  practice-ios
//
//  Created by Jo K on 2017/02/07.
//  Copyright © 2017年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

@interface SliderViewController : UIViewController

@property (weak, nonatomic) GPUImagePicture *gpuImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UISlider *brightnessSlider;

- (IBAction)changed:(UISlider *)sender;
- (IBAction)changedBrightness:(UISlider *)sender;

@end
