//
//  WebViewController.h
//  practice-ios
//
//  Created by ジョ グニョン on 2016/10/19.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface WebViewController : BaseViewController

@property (weak, nonatomic) NSURL *url;
@end
