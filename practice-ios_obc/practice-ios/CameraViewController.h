//
//  CameraViewController.h
//  practice-ios
//
//  Created by Jo K on 2017/02/03.
//  Copyright © 2017年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CameraViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
