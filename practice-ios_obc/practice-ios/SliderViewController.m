//
//  SliderViewController.m
//  practice-ios
//
//  Created by Jo K on 2017/02/07.
//  Copyright © 2017年 F.Workers. All rights reserved.
//

#import "SliderViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface SliderViewController ()
{
    UIImage *baseImage;
}
@end

@implementation SliderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    baseImage = [UIImage imageNamed:@"tibi_12pika.jpg"];
    
    self.slider.minimumValue = 0.0;
    self.slider.maximumValue = 2.0;
    _brightnessSlider.minimumValue = -1.0;
    _brightnessSlider.maximumValue = 1.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changed:(UISlider *)sender {
//    CIImage *ciImage = [[CIImage alloc] initWithImage:_imageView.image];
//    CIFilter *ciFilter = [CIFilter filterWithName:@"CIHueAdjust"
//                                    keysAndValues:kCIInputImageKey, ciImage, @"inputAngle", [NSNumber numberWithFloat:sender.value], nil];
//    
//    CIContext *ciContext = [CIContext contextWithOptions:nil];
//    CGImageRef cgimg = [ciContext createCGImage:[ciFilter outputImage] fromRect:[[ciFilter outputImage] extent]];
//    UIImage *newImage = [UIImage imageWithCGImage:cgimg scale:1.0 orientation:UIImageOrientationUp];
//    CGImageRelease(cgimg);
//    
//    self.imageView.image = newImage;
    
    
    
    
//    NSLog(@"ボタンを押した！");
//    // GUIで設定した画像を取得する
//    UIImage *inputImage = self.imageView.image;
//    
//    // 画像をGPUImageのフォーマットに治す
//    GPUImagePicture *imagePicture = [[GPUImagePicture alloc] initWithImage:inputImage];
//    
//    // セピアフィルターを作る
//    GPUImageSepiaFilter *sepiaFilter = [[GPUImageSepiaFilter alloc] init];
//    
//    // イメージをセピアフィルターにくっつける
//    [imagePicture addTarget:sepiaFilter];
//    
//    // フィルターを実行
//    [imagePicture processImage];
//    // 実行したフィルターから、画像を取得する
//    
//    UIImage *outputImage = [sepiaFilter imageByFilteringImage:inputImage];
    
    
    
    GPUImagePicture *imagePicture = [[GPUImagePicture alloc] initWithImage:_imageView.image];
    GPUImageContrastFilter *contrastFilter = [[GPUImageContrastFilter alloc] init];
    // Contrast ranges from 0.0 to 4.0 (max contrast), with 1.0 as the normal level
    [contrastFilter setContrast:sender.value];
    [imagePicture addTarget:contrastFilter];
    [imagePicture processImage];
//    UIImage *a = [contrastFilter imageFromCurrentlyProcessedOutput];
    UIImage *outputImage = [contrastFilter imageByFilteringImage:baseImage];
    
    // 取得した画像をセットする
    self.imageView.image = outputImage;
}

- (IBAction)changedBrightness:(UISlider *)sender {
//    GPUImagePicture *imagePicture = [[GPUImagePicture alloc] initWithImage:inputImage];
    GPUImageBrightnessFilter *brightnessFilter = [[GPUImageBrightnessFilter alloc] init];
    // Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
    [brightnessFilter setBrightness:sender.value];
//    [imagePicture addTarget:brightnessFilter];
//    [imagePicture processImage];
    UIImage *outputImage = [brightnessFilter imageByFilteringImage:baseImage];
    
    self.imageView.image = outputImage;
}

@end
