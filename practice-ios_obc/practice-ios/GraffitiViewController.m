//
//  GraffitiViewController.m
//  practice-ios
//
//  Created by Jo K on 2017/02/03.
//  Copyright © 2017年 F.Workers. All rights reserved.
//

#import "GraffitiViewController.h"

#import <QuartzCore/QuartzCore.h>

//
// UILabel with outline.
//
@interface OutLineLabel : UILabel

@end

@implementation OutLineLabel

- (void)drawTextInRect:(CGRect)rect
{
    CGSize shadowOffset = self.shadowOffset;
    UIColor *textColor = self.textColor;
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(c, 4);
    CGContextSetLineJoin(c, kCGLineJoinRound);
    
    CGContextSetTextDrawingMode(c, kCGTextStroke);
    self.textColor = [UIColor whiteColor];
    [super drawTextInRect:rect];
    
    CGContextSetTextDrawingMode(c, kCGTextFill);
    self.textColor = textColor;
    self.shadowOffset = CGSizeMake(0, 0);
    [super drawTextInRect:rect];
    
    self.shadowOffset = shadowOffset;
}
@end

@interface GraffitiViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>
{
    UIBezierPath *bezierPath;
    UIImage *lastDrawImage;
    NSMutableArray *undoStack;
    NSMutableArray *redoStack;
    CGPoint lastTouchPoint;
    BOOL firstMovedFlg;
}

@property (nonatomic, strong) UIImageView *pictureView;
@property (nonatomic, strong) UILabel *targetLabel;

@end

@implementation GraffitiViewController
@synthesize pictureView, targetLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    undoStack = [NSMutableArray array];
    redoStack = [NSMutableArray array];
    
    // ボタンのenabledを設定します。
    self.undoBtn.enabled = NO;
    self.redoBtn.enabled = NO;
}

- (IBAction)undoBtnPressed:(id)sender {
    // undoスタックからパスを取り出しredoスタックに追加します。
    UIBezierPath *undoPath = undoStack.lastObject;
    [undoStack removeLastObject];
    [redoStack addObject:undoPath];
    
    // 画面をクリアします。
    lastDrawImage = nil;
    self.canvas.image = nil;
    
    // 画面にパスを描画します。
    for (UIBezierPath *path in undoStack) {
        [self drawLine:path];
        lastDrawImage = self.canvas.image;
    }
    
    // ボタンのenabledを設定します。
    self.undoBtn.enabled = (undoStack.count > 0);
    self.redoBtn.enabled = YES;
}

- (IBAction)redoBtnPressed:(id)sender {
    // redoスタックからパスを取り出しundoスタックに追加します。
    UIBezierPath *redoPath = redoStack.lastObject;
    [redoStack removeLastObject];
    [undoStack addObject:redoPath];
    
    // 画面にパスを描画します。
    [self drawLine:redoPath];
    lastDrawImage = self.canvas.image;
    
    // ボタンのenabledを設定します。
    self.undoBtn.enabled = YES;
    self.redoBtn.enabled = (redoStack.count > 0);
}

- (IBAction)clearBtnPressed:(id)sender {
    // 保持しているパスを全部削除します。
    [undoStack removeAllObjects];
    [redoStack removeAllObjects];
    
    // 画面をクリアします。
    lastDrawImage = nil;
    self.canvas.image = nil;
    
    // ボタンのenabledを設定します。
    self.undoBtn.enabled = NO;
    self.redoBtn.enabled = NO;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // タッチした座標を取得します。
    CGPoint currentPoint = [[touches anyObject] locationInView:self.canvas];
    
    // ボタン上の場合は処理を終了します。
    if (CGRectContainsPoint(self.undoBtn.frame, currentPoint)
        || CGRectContainsPoint(self.redoBtn.frame, currentPoint)
        || CGRectContainsPoint(self.clearBtn.frame, currentPoint)){
        return;
    }
    
    // パスを初期化します。
    bezierPath = [UIBezierPath bezierPath];
    bezierPath.lineCapStyle = kCGLineCapRound;
    bezierPath.lineWidth = 4.0;
    [bezierPath moveToPoint:currentPoint];
    firstMovedFlg = NO;
    
    // タッチした座標を保持します。
    lastTouchPoint = currentPoint;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    // タッチ開始時にパスを初期化していない場合は処理を終了します。
    if (bezierPath == nil){
        return;
    }
    
    // タッチした座標を取得します。
    CGPoint currentPoint = [[touches anyObject] locationInView:self.canvas];
    
    // 最初の移動はスキップします。
    if (!firstMovedFlg){
        firstMovedFlg = YES;
        lastTouchPoint = currentPoint;
        return;
    }
    
    // 中点の座標を取得します。
    CGPoint middlePoint = CGPointMake((lastTouchPoint.x + currentPoint.x) / 2,
                                      (lastTouchPoint.y + currentPoint.y) / 2);
    
    // パスにポイントを追加します。
    [bezierPath addQuadCurveToPoint:middlePoint controlPoint:lastTouchPoint];
    
    // 線を描画します。
    [self drawLine:bezierPath];
    
    // タッチした座標を保持します。
    lastTouchPoint = currentPoint;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // タッチ開始時にパスを初期化していない場合は処理を終了します。
    if (bezierPath == nil){
        return;
    }
    
    // タッチした座標を取得します。
    CGPoint currentPoint = [[touches anyObject] locationInView:self.canvas];
    
    // パスにポイントを追加します。
    [bezierPath addQuadCurveToPoint:currentPoint controlPoint:lastTouchPoint];
    
    // 線を描画します。
    [self drawLine:bezierPath];
    
    // 今回描画した画像を保持します。
    lastDrawImage = self.canvas.image;
    
    // undo用にパスを保持して、redoスタックをクリアします。
    [undoStack addObject:bezierPath];
    [redoStack removeAllObjects];
    bezierPath = nil;
    
    // ボタンのenabledを設定します。
    self.undoBtn.enabled = YES;
    self.redoBtn.enabled = NO;
}

- (void)drawLine:(UIBezierPath *)path
{
    // 非表示の描画領域を生成します。
    UIGraphicsBeginImageContextWithOptions(self.canvas.frame.size, NO, 0.0);
    
    // 描画領域に、前回までに描画した画像を、描画します。
    [lastDrawImage drawAtPoint:CGPointZero];
    
    // 色をセットします。
    [[UIColor redColor] setStroke];
    
    // 線を引きます。
    [path stroke];
    
    // 描画した画像をcanvasにセットして、画面に表示します。
    self.canvas.image = UIGraphicsGetImageFromCurrentImageContext();
    
    // 描画を終了します。
    UIGraphicsEndImageContext();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    // イメージ表示用のViewを生成
    self.pictureView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    [self.view addSubview:self.pictureView];
    
    // コントロールパネルの生成
    [self createControlPanel];
    
    // アウトライン付きのラベルを生成
    self.targetLabel = [[OutLineLabel alloc] init];
    self.targetLabel.backgroundColor = [UIColor clearColor];
    self.targetLabel.font = [UIFont boldSystemFontOfSize:50];
    self.targetLabel.textColor = [UIColor redColor];
    self.targetLabel.center = self.view.center;
    [self.view addSubview:self.targetLabel];
    
    // Pan Gestureで移動できるようにする。
    self.targetLabel.userInteractionEnabled = YES;
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveText:)];
    [self.targetLabel addGestureRecognizer:pan];
}

- (void)createControlPanel
{
    UIView *panel = [[UIView alloc] initWithFrame:CGRectMake(0, 66, [[UIScreen mainScreen] bounds].size.width, 100)];
    panel.backgroundColor = [UIColor darkGrayColor];
    [self.view addSubview:panel];
    
    // 入力フィールド
    UITextField *field = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, [[UIScreen mainScreen] bounds].size.width - 120, 30)];
    field.borderStyle = UITextBorderStyleRoundedRect;
    field.delegate = self;
    [panel addSubview:field];
    
    // カメラボタン
    UIView *photo = [[UIView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 80, 20, 40, 30)];
    photo.layer.cornerRadius = 5.0;
    photo.backgroundColor = [UIColor lightGrayColor];
    [panel addSubview:photo];
    
    // CALayerでアイコンを作る
    CALayer *body = [CALayer layer];
    body.frame = CGRectMake(2, 5, 36, 23);
    body.backgroundColor = [UIColor darkGrayColor].CGColor;
    body.cornerRadius = 5.0;
    [photo.layer addSublayer:body];
    CALayer *shutter = [CALayer layer];
    shutter.frame = CGRectMake(28, 2, 10, 10);
    shutter.backgroundColor = [UIColor darkGrayColor].CGColor;
    shutter.cornerRadius = 2.0;
    [photo.layer addSublayer:shutter];
    CALayer *lens = [CALayer layer];
    lens.frame = CGRectMake(0, 0, 16, 16);
    lens.backgroundColor = [UIColor darkGrayColor].CGColor;
    lens.borderColor = [UIColor lightGrayColor].CGColor;
    lens.borderWidth = 3.0;
    lens.cornerRadius = 8.0;
    lens.position = CGPointMake(20, 18);
    [photo.layer addSublayer:lens];
    
    // カメラボタンのタップでイメージピッカーを開く
    UITapGestureRecognizer *tapicon = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openImagePicker)];
    [photo addGestureRecognizer:tapicon];
    
    // save ボタン
    UILabel *saveBtn = [[UILabel alloc] init];
    saveBtn.text = @"save";
    saveBtn.textColor = [UIColor whiteColor];
    saveBtn.center = CGPointMake([[UIScreen mainScreen] bounds].size.width - 80, 70);
    saveBtn.backgroundColor = [UIColor clearColor];
    [saveBtn sizeToFit];
    [panel addSubview:saveBtn];
    // tapで写真を保存
    saveBtn.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapSave = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(saveImage)];
    [saveBtn addGestureRecognizer:tapSave];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // テキストを編集
    self.targetLabel.text = textField.text;
    [self.targetLabel sizeToFit];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // キーボードのReturnで編集を終了
    self.targetLabel.text = textField.text;
    [self.targetLabel sizeToFit];
    
    [textField resignFirstResponder];
    return NO;
}

- (void)moveText:(UIPanGestureRecognizer *)pgr
{
    // panで動かす
    pgr.view.center = [pgr locationInView:self.view];
}

- (void)openImagePicker
{
    // photo library から選ぶ
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // image picker で選んだ写真を表示する
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.pictureView setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.view bringSubviewToFront:_canvas];
    [self.view bringSubviewToFront:_undoBtn];
    [self.view bringSubviewToFront:_redoBtn];
    [self.view bringSubviewToFront:_clearBtn];
}

- (void)saveImage
{
    // photo libraryに保存する。
    UIGraphicsBeginImageContext(self.pictureView.frame.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, NULL);
    UIGraphicsEndImageContext();
    
    
    // shutter effectを付けてみる
    UIView *stop = [[UIView alloc] initWithFrame:CGRectMake(0, 500, 320, 500)];
    UIView *sbottom = [[UIView alloc] initWithFrame:CGRectMake(0, -500, 320, 500)];
    stop.backgroundColor = [UIColor blackColor];
    sbottom.backgroundColor = [UIColor blackColor];
    [self.view addSubview:stop];
    [self.view addSubview:sbottom];
    
    [UIView animateWithDuration:0.3 animations:^{
        stop.center = self.view.center;
        sbottom.center = self.view.center;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            stop.center = CGPointMake(160, 800);
            sbottom.center = CGPointMake(160, -500);
        } completion:^(BOOL finished) {
            [stop removeFromSuperview];
            [sbottom removeFromSuperview];
        }];
    }];
    
}

@end
