//
//  GraffitiViewController.h
//  practice-ios
//
//  Created by Jo K on 2017/02/03.
//  Copyright © 2017年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraffitiViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *canvas;
@property (weak, nonatomic) IBOutlet UIButton *undoBtn;
@property (weak, nonatomic) IBOutlet UIButton *redoBtn;
@property (weak, nonatomic) IBOutlet UIButton *clearBtn;

- (IBAction)undoBtnPressed:(id)sender;
- (IBAction)redoBtnPressed:(id)sender;
- (IBAction)clearBtnPressed:(id)sender;

@end
