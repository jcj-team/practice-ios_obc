//
//  ImageViewController.h
//  practice-ios
//
//  Created by Jo K on 2016/11/04.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import "BaseViewController.h"

@interface ImageViewController : BaseViewController <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *blankView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
