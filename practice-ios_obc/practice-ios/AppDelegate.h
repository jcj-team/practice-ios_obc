//
//  AppDelegate.h
//  practice-ios
//
//  Created by ジョ グニョン on 2016/10/09.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

