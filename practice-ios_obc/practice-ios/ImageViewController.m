//
//  ImageViewController.m
//  practice-ios
//
//  Created by Jo K on 2016/11/04.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import "ImageViewController.h"
#import "WebViewController.h"

@interface ImageViewController ()
{
    UITextView *_textView;
    UIAlertController *_useAgreementAlertAction;
}
@end

@implementation ImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _blankView.userInteractionEnabled = YES;
    _imageView.userInteractionEnabled = YES;
    
    _imageView.layer.cornerRadius = _imageView.frame.size.width / 2.0f;
    _imageView.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if ([event touchesForView:_blankView]) {
        _blankView.backgroundColor = [UIColor blackColor];
        _blankView.alpha = 0.7f;
    } else if ([event touchesForView:_imageView]) {
        _imageView.backgroundColor = [UIColor blackColor];
        _imageView.alpha = 0.7f;
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if ([event touchesForView:_blankView]) {
        _blankView.backgroundColor = [UIColor clearColor];
        _blankView.alpha = 1.0f;
    } else if ([event touchesForView:_imageView]) {
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.alpha = 1.0f;
    }
}

- (void)useAgreementAlertAction:(NSUInteger)selectedPage chatType:(NSUInteger)type
{
    _useAgreementAlertAction = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"\n\n\n\n\n\n\n\n\n"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [_useAgreementAlertAction addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
//                                                                   [self checkMonthlyMembership:selectedPage chatType:type];
                                                               }]];
    
    [_useAgreementAlertAction addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                 style:UIAlertActionStyleCancel
                                                               handler:nil]];
    
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(8, 15, 255, 200)];
    _textView.userInteractionEnabled = YES;
    _textView.editable = NO;
    _textView.backgroundColor = [UIColor clearColor];
    _textView.layer.cornerRadius = 8;
    _textView.delegate = self;
    
    NSMutableAttributedString *attributedString = [self setupMutableText];
    
    _textView.attributedText = attributedString;
    
    [_useAgreementAlertAction.view addSubview:_textView];
    
    [self presentViewController:_useAgreementAlertAction animated:YES completion:nil];
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    NSLog(@"URL = %@", URL.absoluteString);
    
    [self dismissViewControllerAnimated:_useAgreementAlertAction completion:^{
        WebViewController *webViewController = [WebViewController viewControllerWithStoryBoard];
        webViewController.url = [NSURL URLWithString:@"http://www.naver.com"];
//        webViewController.showStartButton = NO;
//        webViewController.navigationBarTitle = NSLocalizedString(@"TitleWithTermsOfService", nil);
        webViewController.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:webViewController animated:YES];
    }];
    
    return NO;
}
- (IBAction)clickButton:(id)sender {
//    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSURL *url;
    
//    if (iOSVersion >= 10.0) {
        url = [NSURL URLWithString:@"App-Prefs:root=TWITTER"];
        [[UIApplication sharedApplication] openURL:url];
//    } else {
//        url = [NSURL URLWithString:@"prefs:root=TWITTER"];
//        [[UIApplication sharedApplication] openURL:url];
//    }
}

- (NSMutableAttributedString *)setupMutableText
{
    NSString *useAgreementAlertMessage = @"｢診察｣やそれに伴う｢処方箋・医薬品発送｣を利用するには、選択した医師に過去、対面での診察を受けたことがある場合か、近日中に直接医師の勤務先に訪れ対面での診察を必ず受ける必要があります。\n\n利用規約\n\n規約に同意した上で利用しますか？";
    NSString *useAgreementAlertMessageTitle = @"｢診察｣やそれに伴う｢処方箋・医薬品発送｣を利用するには、選択した医師に過去、対面での診察を受けたことがある場合か、近日中に直接医師の勤務先に訪れ対面での診察を必ず受ける必要があります。";
    NSString *linkText = @"利用規約";
    NSString *useAgreementAlertMessageConfirmation = @"規約に同意した上で利用しますか？";
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:useAgreementAlertMessage];
    NSRange title = [useAgreementAlertMessage rangeOfString:useAgreementAlertMessageTitle];
    NSRange link = [useAgreementAlertMessage rangeOfString:linkText];
    NSRange confirmation = [useAgreementAlertMessage rangeOfString:useAgreementAlertMessageConfirmation];
    
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    paragrahStyle.alignment = NSTextAlignmentCenter;
    paragrahStyle.hyphenationFactor = 0.9;
    paragrahStyle.lineSpacing = 3.0f;
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:title];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:link];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:confirmation];
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:title];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:title];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:link];
    [attributedString addAttribute:NSLinkAttributeName value:@"https://smadoc.com" range:link];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:link];
    
    //    NSDictionary *stringAttributes1 = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:13.0f] };
    //    NSAttributedString *string1 = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"AlertMessage_UseAgreement", nil)
    //                                                                  attributes:stringAttributes1];
    //
    //    NSString *linkText = @"\n利用規約\n";
    //    NSDictionary *stringAttributes2 = @{ NSFontAttributeName : [UIFont systemFontOfSize:16.0f],
    //                                         NSForegroundColorAttributeName:[UIColor redColor] };
    //    NSAttributedString *string2 = [[NSAttributedString alloc] initWithString:linkText
    //                                                                  attributes:stringAttributes2];
    //
    //    NSDictionary *stringAttributes3 = @{ NSFontAttributeName : [UIFont systemFontOfSize:12.0f] };
    //    NSAttributedString *string3 = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"AlertMessage_UseAgreement_Confirmation", nil)
    //                                                                  attributes:stringAttributes3];
    //
    //    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] init];
    //    [mutableAttributedString appendAttributedString:string1];
    //    [mutableAttributedString appendAttributedString:string2];
    //    [mutableAttributedString appendAttributedString:string3];

    return attributedString;
}

@end
