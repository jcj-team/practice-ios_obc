//
//  BaseViewController.h
//  practice-ios
//
//  Created by Jo K on 2016/11/03.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

//+ (instancetype)viewControllerWithXib __attribute__ ((deprecated));
+ (instancetype)viewControllerWithXib;
+ (instancetype)viewControllerWithStoryBoard;

- (void)showCABasicAnimationWhichRotates:(UIImageView *)image;
- (void)showCABasicAnimationWhichFlashing:(UILabel *)label;

@end
