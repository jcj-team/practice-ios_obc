//
//  SecondViewController.m
//  practice-ios
//
//  Created by ジョ グニョン on 2016/10/13.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import "StopwatchViewController.h"

@interface StopwatchViewController ()

@end

@implementation StopwatchViewController
{
    NSTimer *_stopwatch;
    IBOutlet UILabel *_stopwatchLabel;
    
    float _stopwatchCount;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewControllerDelegate

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSTimer

- (void)stopwatchStart
{
    _stopwatch = [NSTimer scheduledTimerWithTimeInterval:0.01
                                              target:self
                                            selector:@selector(stopwatch)
                                            userInfo:nil
                                             repeats:YES
              ];
}

- (void)stopwatch
{
    _stopwatchCount = _stopwatchCount + 0.01f;
    float second = fmodf(_stopwatchCount, 60);
    int minute = _stopwatchCount / 60;
    int hour = minute / 60;
    
    NSString *countUp = [NSString stringWithFormat:@"%02d:%02d:%05.2f", hour, minute, second];
    
    _stopwatchLabel.text = countUp;
    
    //    NSTimeInterval dateDiff = [SDUtils getUnixTimeFromDate:[NSDate date]];
    //    int hour   = dateDiff / (60 * 60);
    //    int minute = fmod((dateDiff / 60) ,60);
    //    int second = fmod(dateDiff ,60);
    //    int miliSec = (dateDiff - floor(dateDiff)) * 1000;
    //    _remainingTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d.%03d", minute, second, miliSec];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIAction

- (IBAction)clickedTimerStartButton:(id)sender
{
    if (![_stopwatch isValid]) {
        [self stopwatchStart];
    }
}

- (IBAction)clickedTimerStopButton:(id)sender
{
    if ([_stopwatch isValid]) {
        [_stopwatch invalidate];
    }
}

- (IBAction)clickedTimerResetButton:(id)sender
{
    
}
@end
