//
//  MainMenuViewController.m
//  practice-ios
//
//  Created by ジョ グニョン on 2016/10/09.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import "MainMenuViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController
{
    IBOutlet UIImageView *_animationImage;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewControllerDelegate

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showCABasicAnimationWhichRotates:_animationImage];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_animationImage.layer removeAllAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)showWaitView
{
    [UIView animateWithDuration:1.0
                          delay:0.3
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _animationImage.transform = CGAffineTransformMakeRotation((180.0f * M_PI) / 180.0);
                     }
                     completion:nil];
}

@end
