//
//  main.m
//  practice-ios
//
//  Created by ジョ　グニョン on 2016/10/09.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
