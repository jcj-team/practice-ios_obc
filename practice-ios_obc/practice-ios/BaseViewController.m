//
//  BaseViewController.m
//  practice-ios
//
//  Created by Jo K on 2016/11/03.
//  Copyright © 2016年 F.Workers. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewControllerDelegate

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Push이동시 배경애니메이션의 표시관련
//    self.tabBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////
#pragma mark - Initialization

+ (instancetype)viewControllerWithXib;
{
    return [[[self class] alloc] initWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (instancetype)viewControllerWithStoryBoard
{
    UIStoryboard *myStoryBoard = [UIStoryboard storyboardWithName:NSStringFromClass([self class]) bundle:nil];
    return [myStoryBoard instantiateInitialViewController];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - CABasicAnimation

- (void)showCABasicAnimationWhichRotates:(UIImageView *)image
{
    CALayer *layer = image.layer;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    
    //アニメーション時間
    animation.duration = 2.0;
    
    //アニメーションの繰り返し回数
    animation.repeatCount = MAXFLOAT;
    
    // 効果を累積
    animation.cumulative = YES;
    
    //アニメーションの開始時間
    //    animation.beginTime = CACurrentMediaTime();
    
    //アニメーションのイージングを制御
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    //アニメーションの開始と終了の値を設定
    //        animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(0, 0)];
    animation.toValue = [NSNumber numberWithFloat:M_PI * 2];
    
    //アニメーション終了時、元の状態に戻すか否かの設定（サンプルではアニメーション後はそのまま）
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
    //アニメーションキーを設定（任意の名前）
    [layer addAnimation:animation forKey:@"ImageViewRotation"];
}

- (void)showCABasicAnimationWhichFlashing:(UILabel *)label
{
    CALayer *layer = label.layer;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    
    animation.duration = 1.0;
    animation.repeatCount = MAXFLOAT;
    animation.autoreverses = YES;
    
    animation.fromValue = [NSNumber numberWithFloat:0.0];
    animation.toValue = [NSNumber numberWithFloat:2.0];
    
    [layer addAnimation:animation forKey:@"animateOpacity"];
}

@end
