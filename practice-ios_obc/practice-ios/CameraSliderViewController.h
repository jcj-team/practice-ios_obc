//
//  CameraSliderViewController.h
//  practice-ios
//
//  Created by Jo K on 2017/02/06.
//  Copyright © 2017年 F.Workers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CameraSliderViewController : UIViewController

@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (weak, nonatomic) IBOutlet UISlider *exposureSlider;

@end
